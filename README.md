Értékelés Gulyás János felvételi feladatához
============================================

Első benyomások
---------------

- A forráskódot Google Drive-on osztotta meg a jelölt, professzionálisabbnak tűnt volna valamilyen verziókezelő rendszer (pl. git) használata. (Számos ingyenesen használható repozitórium kezelő érhető el, pl.: github, gitlab.)
- A megosztott archívumban fordítási kimenetek (Build mappa) is megtalálható. Ezek megosztása tipikusan felesleges.
- A forrásfájlok UCS-2 kódolásúak, ez véleményem szerint kevésbé szerencsés. UTF-8, vagy ékezetes kommentek híján ANSI kódolás szerencsésebb kompatibilitás szempontjából.

Fordítás
--------
- A CMakeLists.txt fájl egyetlen library-t definiál, ami problémamentesen lefordul a projekt betöltése után, azonban minta alkalmazás nélkül nem derül fény az esetleges hibákra.
	
Implementáció
-------------
- A CMake projekt nem definiál futtatható alkalmazást és ehhez források sem találhatóak.
- Minta applikáció hozzáadása után fordítási hibát okoznak a header-ben definiált nem-konstans `s_x, s_y, s_z` változók. Ezeket célszerűbb lett volna az`Input_Low` osztály adattagjaiként, vagy lokális változójaként definiálni.
- Sikeres fordítás után az `Input_Low` és `Input_High` osztályok kimenetei különböznek, a hard-coded `s_x, s_y, s_z` kezdeti értékek miatt.
- A library nem ad interface-t a kezdeti jel-értékek beállítására, ezt jó lett volna a `calculate` függvényben megvalósítani.
- Ugyanolyan kezdeti értékek mellett is különböznek az eredmények, ennek oka az alábbi sorokban levő elírás
```
    res1 += a_xtmp * w_xtmp*cos(a_xtmp*t_i)*d_t;
    res2 += a_ytmp * w_ytmp*cos(a_ytmp*t_i)*d_t;
    res3 += a_ztmp * w_ztmp*cos(a_ztmp*t_i)*d_t;
                                ^^^^^^= amplitúdók használata frekvencia helyett!
```

- Nem különül el a két platform szint. A `<thread>` std library includeolását célszerű lenne valahogy kapcsolhatóvá tenni.
- A belső logika kétszer is implementálva van (`Input_Low` és `Input_High` osztályokban).
- A `Input_High::calculate()` függvényben felesleges extra változókat kapnak el a lambdák, a `this` helyett.
- A library "float" típust használ ami kis időlépések vagy kicsi frekvenica és amplitúdó esetén nem elégséges. Ezt a típust célszerű lett volna legalább typedef-fel konfigurálhatóvá tenni.
- Minta applikáción kívül teszt sincs a library kipróbálására.
	
Összegzés
---------
- Minta applikáció vagy teszt nélkül kérdéses, hogy a jelölt hogyan tesztelte saját implementációját. Ha tesztelte, akkor miért nem csatolta megoldásához a szükséges forrásfájlokat?
- Mivel az osztályok neve tartalmazza a platform szintet, így nem lehetséges ugyanazt az applikációt (ugyanazt a forrást) egyszerre, mindkét library-vel szemben lefordítani.
- Nem lehet konfigurálni a library-t (platformot választani) a CMake projektben.