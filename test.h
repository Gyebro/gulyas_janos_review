/* test.h : 
Az oszcillátor tesztprogarm header file-ja.

Alap logika: két osztály köré épül a program, az egyik osztály az alacsony, a másik a magas szintű platformhoz tartozik. Az objektum létrehozásánál kell megadni az oszcillátor paramétereit.

Jelen megvalósításban a csatornák frekvenciáit és amplitúdóit kérjük, a kezdeti értékek előre definiáltak (kis módosítással ez is bekérhető persze).

Attól függően, hogy milyen a platformom, fog egy vagy több szálon futni a számítás.
*/
#pragma once

class Input_Low;
class Input_High;

#include "config.h"

#include <cmath>
#include <thread>

const flt PI = 3.141592653589793238463;
const flt d_t = 0.001; // Időlépés a számításhoz

class Input_Low // Alacsony szintű platform osztálya. 0-100 között legyenek az értékek
{
public:
	//attributes - ebben a megvalósításban nem pre-defined értékekkel dolgozunk, hanem az osztály létrehozásánál adhatom meg az adott oszcilloszkóp paramétereit
	flt w_x;
	flt w_y;
	flt w_z;
	flt a_x;
	flt a_y;
	flt a_z;

	//methods
	Input_Low(flt w_x_user=0, flt w_y_user=0, flt w_z_user=0, flt a_x_user=0, flt a_y_user=0, flt a_z_user=0); // Konstruktor fv
	flt calculate(); // A számítást végző fv
	
};

class Input_High // Magas szintű platform osztálya. 0-100 között legyenek az értékek
{
public:
	//attributes
	flt w_x;
	flt w_y;
	flt w_z;
	flt a_x;
	flt a_y;
	flt a_z;

	//methods
	Input_High(flt w_x_user = 0, flt w_y_user = 0, flt w_z_user = 0, flt a_x_user = 0, flt a_y_user = 0, flt a_z_user = 0); // Konstruktor fv
	flt calculate(); // Három szálon futó számítófv
};

		