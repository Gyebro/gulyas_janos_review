/* 
test.cpp
*/

#include "test.h"

using namespace std;

//Konstruktorfüggvény deklarálása

Input_Low::Input_Low(flt w_x_user, flt w_y_user, flt w_z_user, flt a_x_user, flt a_y_user, flt a_z_user)
	: w_x{ w_x_user }, w_y{ w_y_user }, w_z{ w_z_user }, a_x{ a_x_user }, a_y{ a_y_user }, a_z{ a_z_user }
{
}

// Az alacsony szintű (pl.embedded) platformhoz tartozó számítási függvény
flt Input_Low::calculate()
{
    flt s_x = 0; // kezdeti értékek a csatornákra
    flt s_y = 0;
    flt s_z = 0;
	int i = 0;
	flt t_i = 0;

	while ((i*d_t) < (20 * PI)) // számítandó időtartomány
	{
		t_i = i * d_t; // Időlépés érvényesítése
		s_x += a_x * w_x*cos(w_x*t_i)*d_t;
		s_y += a_y * w_y*cos(w_y*t_i)*d_t;
		s_z += a_z * w_z*cos(w_z*t_i)*d_t;
		i++;
	}

	return(sqrt(s_x*s_x + s_y *s_y + s_z *s_z));
}

//Konstruktorfüggvény deklarálása

Input_High::Input_High(flt w_x_user, flt w_y_user, flt w_z_user, flt a_x_user, flt a_y_user, flt a_z_user)
	: w_x{ w_x_user }, w_y{ w_y_user }, w_z{ w_z_user }, a_x{ a_x_user }, a_y{ a_y_user }, a_z{ a_z_user }
{
}


// A magas szintű platform több szálon futó számítófüggvénye. Csatornánként külön szálon számítja a szummákat.
flt Input_High::calculate()
{
	flt res1 = 0;
	flt res2 = 0;
	flt res3 = 0;
	flt a_xtmp = a_x;
	flt a_ytmp = a_y;
	flt a_ztmp = a_z;
	flt w_xtmp = w_x;
	flt w_ytmp = w_y;
	flt w_ztmp = w_z;

	std::thread t1([&res1, &a_xtmp, &w_xtmp] // lambda függvénnyel futtatom a szálakat
	{
		int i = 0;
		flt t_i = 0;

		while ((i*d_t) < (20 * PI)) // számítandó időtartomány
		{
			t_i = i * d_t; // Időlépés érvényesítése
			res1 += a_xtmp * w_xtmp*cos(a_xtmp*t_i)*d_t;
			i++;
		}
	});

	std::thread t2([&res2, &a_ytmp, &w_ytmp]
	{
		int i = 0;
		flt t_i = 0;

		while ((i*d_t) < (20 * PI)) 
		{
			t_i = i * d_t;
			res2 += a_ytmp * w_ytmp*cos(a_ytmp*t_i)*d_t;
			i++;
		}
	});

	std::thread t3([&res3, &a_ztmp, &w_ztmp]
	{
		int i = 0;
		flt t_i = 0;

		while ((i*d_t) < (20 * PI)) 
		{
			t_i = i * d_t; 
			res3 += a_ztmp * w_ztmp*cos(a_ztmp*t_i)*d_t;
			i++;
		}
	});
	

	t1.join(); // Szálak lezárása
	t2.join();
	t3.join();

	// Miután befejeződtek a számítások, kiszámoljuk a végeredményt
	return(sqrt(res1 * res1 + res2 *res2 + res3 *res3));
}


