#include <iostream>
#include "test.h"

using namespace std;

int main() {
    cout << "Testing multi-platform library" << endl;

    Input oscillator(1.0,2.0,10.0,20.0,10.0,2.0);
    cout << "Result: " << oscillator.calculate() << endl;

    return 0;
}

